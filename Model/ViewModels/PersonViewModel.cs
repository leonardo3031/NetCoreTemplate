﻿using System;
using System.Collections.Generic;
using System.Text;
using Model.Models;

namespace Model.ViewModels
{
    public class PersonViewModel : Person
    {
        public IEnumerable<Person> Persons { get; set; }
    }
}
