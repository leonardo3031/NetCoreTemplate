using Microsoft.EntityFrameworkCore;
using SecuritySGD.Entities;

namespace SecuritySGD.DataLayer
{
    public class SecurityDbContext : DbContext
    {
        public SecurityDbContext(DbContextOptions<SecurityDbContext> options) : base(options)
        {
        }

        public DbSet<User> User { set; get; }
        //public DbSet<Role> Role { set; get; }
        //public DbSet<UserRole> UserRole { get; set; }
        public DbSet<UserToken> UserToken { get; set; }
        public DbSet<AccessForm> AccessForm { get; set; }
        //public DbSet<AccessGroup> AccessGroup { get; set; }
        public DbSet<Menu> Menu { get; set; }
        public DbSet<UserAccess> UserAccess { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            // it should be placed here, otherwise it will rewrite the following settings!
            base.OnModelCreating(builder);

            builder.Entity<User>(entity => { entity.HasIndex(e => e.Username).IsUnique(); });
            //builder.Entity<Role>(entity => { entity.HasIndex(e => e.Name).IsUnique(); });
            builder.Entity<AccessForm>(entity => { entity.HasIndex(e => e.FormName).IsUnique(); });

            builder.Entity<UserAccess>(entity => { entity.HasKey(t => new {t.Action, t.AccessFormId, t.UserId}); });


            //builder.Entity<UserRole>(entity =>
            //{
            //    entity.HasKey(e => new {e.UserId, e.RoleId});
            //    entity.HasIndex(e => e.UserId);
            //    entity.HasIndex(e => e.RoleId);
            //    entity.Property(e => e.UserId);
            //    entity.Property(e => e.RoleId);
            //});

            //builder.Entity<AccessGroup>(entity =>
            //{
            //    entity.HasMany(e => e.AccessForm).WithOne(e => e.AccessGroup).HasForeignKey(d => d.AccessGroupId);
            //});

            builder.Entity<AccessForm>(entity =>
            {
                entity.HasMany(e => e.Menu).WithOne(e => e.AccessForm).HasForeignKey(d => d.AccessFormId);
            });
        }
    }
}