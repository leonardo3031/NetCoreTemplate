using Microsoft.EntityFrameworkCore.Design;
using SecuritySGD.Helper;

namespace SecuritySGD.DataLayer
{
    public class SecurityDbContextFactory : IDesignTimeDbContextFactory<SecurityDbContext>
    {
        public SecurityDbContext CreateDbContext(string[] args)
        {
            return App.GetContext<SecurityDbContext>();
        }
    }
}