﻿using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using SecuritySGD.DataLayer;
using SecuritySGD.Entities;
using SecuritySGD.Helper;
using SecuritySGD.Utils;

namespace SecuritySGD.Services
{
    public interface IAuthService
    {
        UserModel FindUser(string username, string password);
        UserModel FindUser(int userId);
        void UpdateUserLastActivityDate(int userId);
        CustomAccess ValidateAccess(string formName, int userId);
        void SetUserCredentials(UserModel model);
        void Validate(TokenValidatedContext context);
        UserModel CreateUser(UserModel user);
    }

    public class AuthService : IAuthService
    {
        private readonly SecurityDbContext _db;
        private readonly ITokenStoreService _tokenStoreService;

        public AuthService(SecurityDbContext context, ITokenStoreService tokenStoreService)
        {
            _db = context;

            _tokenStoreService = tokenStoreService;
            _tokenStoreService.CheckArgumentIsNull(nameof(_tokenStoreService));
        }

        public UserModel FindUser(int userId)
        {
            return _db.User.Find(userId).ToUserModel();
        }

        public UserModel FindUser(string username, string password)
        {
            var passwordHash = SecurityParams.GetSha256Hash(password);

            return _db.User.FirstOrDefault(x => x.Username == username && x.Password == passwordHash).ToUserModel();
        }

        public void UpdateUserLastActivityDate(int userId)
        {
            var user = FindUser(userId);

            if (user.LastLoggedIn != null)
            {
                var updateLastActivityDate = TimeSpan.FromMinutes(2);
                var currentUtc = DateTimeOffset.UtcNow;
                var timeElapsed = currentUtc.Subtract(user.LastLoggedIn.Value);

                if (timeElapsed < updateLastActivityDate)
                    return;
            }

            user.LastLoggedIn = DateTimeOffset.UtcNow;

            _db.SaveChanges();
        }

        public CustomAccess ValidateAccess(string formName, int userId)
        {
            var query = (from a in _db.UserAccess
                    join b in _db.AccessForm
                        on a.AccessFormId equals b.Id
                    where b.FormName == formName && a.UserId == userId
                    select new
                    {
                        a.UserId,
                        b.FormName,
                        a.Action
                    }).GroupBy(a => new {a.UserId, a.FormName})
                .Select(g => new CustomAccess
                {
                    FormName = g.Key.FormName,
                    Action = (CrudAction) g.Sum(s => s.Action)
                });

            return query.FirstOrDefault();
        }

        public void SetUserCredentials(UserModel model)
        {
            model.Password = SecurityParams.GetSha256Hash(model.Password);
            model.SerialNumber = Guid.NewGuid().ToString("N");
            model.IsActive = true;
        }

        public void Validate(TokenValidatedContext context)
        {
            var claimsIdentity = context.Principal.Identity as ClaimsIdentity;

            if (claimsIdentity?.Claims == null || !claimsIdentity.Claims.Any())
                throw new ExceptionResult("This is not our issued token. It has no claims.", 401);

            var serialNumberClaim = claimsIdentity.FindFirst(ClaimTypes.SerialNumber);

            if (serialNumberClaim == null)
                throw new ExceptionResult("This is not our issued token. It has no serial.", 401);

            var userIdString = claimsIdentity.FindFirst(ClaimTypes.UserData).Value;

            if (!int.TryParse(userIdString, out var userId))
                throw new ExceptionResult("This is not our issued token. It has no user-id.", 401);

            var user = FindUser(userId);

            // user has changed his/her password/roles/stat/IsActive
            if (user == null || user.SerialNumber != serialNumberClaim.Value || !user.IsActive)
                throw new ExceptionResult("El token ha expirado. Por favor, inicie sesión nuevamente.", 401);

            if (!(context.SecurityToken is JwtSecurityToken accessToken) ||
                string.IsNullOrWhiteSpace(accessToken.RawData) ||
                !_tokenStoreService.IsValidToken(accessToken.RawData, userId))
                throw new ExceptionResult("El token no esta en nuestra base de datos.", 401);

            UpdateUserLastActivityDate(userId);
        }

        public UserModel CreateUser(UserModel user)
        {
            _db.User.Add(user.GetEntity());

            if (_db.SaveChanges() > 0)
            {
                return FindUser(user.Username, user.Password);
            }

            throw new ExceptionResult("Error al crear el usuario.");
        }
    }
}