using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using SecuritySGD.DataLayer;
using SecuritySGD.Entities;
using SecuritySGD.Helper;
using SecuritySGD.Utils;

namespace SecuritySGD.Services
{
    public interface ITokenStoreService
    {
        void AddUserToken(
            UserModel user, string refreshToken, string accessToken,
            DateTimeOffset refreshTokenExpiresDateTime, DateTimeOffset accessTokenExpiresDateTime);

        void AddUserToken(UserToken userToken);
        bool IsValidToken(string accessToken, int userId);
        void DeleteExpiredTokens();
        UserTokenModel FindToken(string refreshToken);
        void DeleteToken(string refreshToken);
        void InvalidateUserTokens(int userId);
        void CreateJwtTokens(UserModel user);
    }

    public class TokenStoreService : ITokenStoreService
    {
        private readonly IOptionsSnapshot<BearerTokensOptions> _configuration;
        private readonly SecurityDbContext _db;

        public TokenStoreService(
            SecurityDbContext context,
            IOptionsSnapshot<BearerTokensOptions> configuration)
        {
            _db = context;
            _configuration = configuration;
            _configuration.CheckArgumentIsNull(nameof(configuration));
        }

        public void AddUserToken(UserToken userToken)
        {
            InvalidateUserTokens(userToken.UserId);
            _db.UserToken.Add(userToken);
        }

        public void AddUserToken(
            UserModel user, string refreshToken, string accessToken,
            DateTimeOffset refreshTokenExpiresDateTime, DateTimeOffset accessTokenExpiresDateTime)
        {
            var token = new UserToken
            {
                UserId = user.Id,
                // Refresh token handles should be treated as secrets and should be stored hashed
                RefreshTokenIdHash = SecurityParams.GetSha256Hash(refreshToken),
                AccessTokenHash = SecurityParams.GetSha256Hash(accessToken),
                RefreshTokenExpiresDateTime = refreshTokenExpiresDateTime,
                AccessTokenExpiresDateTime = accessTokenExpiresDateTime
            };

            AddUserToken(token);
        }

        public void DeleteExpiredTokens()
        {
            _db.UserToken.RemoveRange(_db.UserToken.Where(x =>
                x.RefreshTokenExpiresDateTime < DateTimeOffset.UtcNow));
        }

        public void DeleteToken(string refreshToken)
        {
            var token = FindToken(refreshToken);

            if (token != null)
                _db.UserToken.Remove(token.ToUserToken());
        }

        public UserTokenModel FindToken(string refreshToken)
        {
            if (string.IsNullOrWhiteSpace(refreshToken))
                return null;

            var refreshTokenIdHash = SecurityParams.GetSha256Hash(refreshToken);

            return _db.UserToken.FirstOrDefault(x => x.RefreshTokenIdHash == refreshTokenIdHash).ToUserTokenModel();
        }

        public void InvalidateUserTokens(int userId)
        {
            _db.UserToken.RemoveRange(_db.UserToken.Where(x => x.UserId == userId));
        }

        public bool IsValidToken(string accessToken, int userId)
        {
            var accessTokenHash = SecurityParams.GetSha256Hash(accessToken);

            var userToken = _db.UserToken.FirstOrDefault(
                x => x.AccessTokenHash == accessTokenHash && x.UserId == userId);

            return userToken?.AccessTokenExpiresDateTime >= DateTime.UtcNow;
        }

        public void CreateJwtTokens(UserModel user)
        {
            var now = DateTimeOffset.UtcNow;
            var accessTokenExpiresDateTime = now.AddMinutes(_configuration.Value.AccessTokenExpirationMinutes);
            var refreshTokenExpiresDateTime = now.AddMinutes(_configuration.Value.RefreshTokenExpirationMinutes);
            var accessToken = CreateAccessToken(user, accessTokenExpiresDateTime.UtcDateTime);
            var refreshToken = Guid.NewGuid().ToString().Replace("-", "");

            AddUserToken(user, refreshToken, accessToken, refreshTokenExpiresDateTime,
                accessTokenExpiresDateTime);

            _db.SaveChanges();

            user.Token = $"{accessToken}";
            user.RefreshToken = refreshToken;
        }

        private string CreateAccessToken(UserModel user, DateTime expires)
        {
            var claims = new List<Claim>
            {
                // Unique Id for all Jwt tokes
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                // Issuer
                new Claim(JwtRegisteredClaimNames.Iss, _configuration.Value.Issuer),
                // Issued at
                new Claim(JwtRegisteredClaimNames.Iat, DateTime.UtcNow.ToUnixEpochDate().ToString(),
                    ClaimValueTypes.Integer64),
                new Claim(ClaimTypes.NameIdentifier, user.Id.ToString()),
                new Claim(ClaimTypes.Name, user.Username),
                new Claim(SecurityParams.UserId, user.Id.ToString()),
                new Claim(SecurityParams.DisplayName, user.DisplayName),
                new Claim(SecurityParams.User, JsonConvert.SerializeObject(user)),
                // to invalidate the cookie
                new Claim(ClaimTypes.SerialNumber, user.SerialNumber),
                // custom data
                new Claim(ClaimTypes.UserData, user.Id.ToString())
            };

            // add roles
            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration.Value.Key));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
            var token = new JwtSecurityToken(
                _configuration.Value.Issuer,
                _configuration.Value.Audience,
                claims,
                DateTime.UtcNow,
                expires,
                creds);

            return new JwtSecurityTokenHandler().WriteToken(token);
        }
    }
}