using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using SecuritySGD.DataLayer;
using SecuritySGD.Entities;
using SecuritySGD.Helper;
using SecuritySGD.Utils;

namespace SecuritySGD.Services
{
    public class SecurityInitializerService
    {
        private readonly IServiceScopeFactory _scopeFactory;

        public SecurityInitializerService(
            IServiceScopeFactory scopeFactory)
        {
            _scopeFactory = scopeFactory;
            _scopeFactory.CheckArgumentIsNull(nameof(_scopeFactory));
        }

        public void Initialize()
        {
            using (var serviceScope = _scopeFactory.CreateScope())
            {
                using (var context = serviceScope.ServiceProvider.GetService<SecurityDbContext>())
                {
                    context.Database.Migrate();
                }
            }
        }

        public void SeedData()
        {
            using (var serviceScope = _scopeFactory.CreateScope())
            {
                using (var context = serviceScope.ServiceProvider.GetService<SecurityDbContext>())
                {
                    context.User.AddOrUpdate(u => new {u.Username}, new User
                    {
                        Username = "sa",
                        DisplayName = "System Administrator",
                        IsActive = true,
                        LastLoggedIn = null,
                        Password = SecurityParams.GetSha256Hash("123456"),
                        SerialNumber = "feeed551178843079c5e4beab51fc171"
                    });
                    context.SaveChanges();
                }
            }
        }
    }
}