using System.Security.Claims;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using SecuritySGD.DataLayer;
using SecuritySGD.Entities;
using SecuritySGD.Helper;
using SecuritySGD.Services;
using SecuritySGD.Utils;

namespace SecuritySGD.Controllers
{
    [Route("[controller]")]
    [EnableCors("CorsPolicy")]
    [Produces("application/json")]
    public class SecurityController : Controller
    {
        private readonly IAuthService _authService;
        private readonly SecurityDbContext _db;
        private readonly ITokenStoreService _tokenStoreService;

        public SecurityController(
            IAuthService authService,
            ITokenStoreService tokenStoreService,
            SecurityDbContext context)
        {
            _authService = authService;
            _authService.CheckArgumentIsNull(nameof(authService));

            _tokenStoreService = tokenStoreService;
            _tokenStoreService.CheckArgumentIsNull(nameof(_tokenStoreService));
            _db = context;
        }

        [HttpPost("[action]")]
        public JsonResult<UserModel> Login([FromBody] UserModel data)
        {
            if (string.IsNullOrEmpty(data?.Password) || string.IsNullOrEmpty(data.Username))
                throw new ExceptionResult("Usuario o contraseņa incorrecto", 202);

            var user = _authService.FindUser(data.Username, data.Password);
            if (user == null || !user.IsActive)
                throw new ExceptionResult("Usuario o contraseņa incorrecto", 202);
            this.AfterLogin(user);
            _tokenStoreService.CreateJwtTokens(user);

            return new JsonResult<UserModel>(user);
        }

        [HttpPost("[action]")]
        [CustomAuthorize]
        public JsonResult<UserModel> RefreshToken([FromBody] JToken jsonBody)
        {
            var refreshToken = jsonBody.Value<string>("refreshToken");

            if (string.IsNullOrWhiteSpace(refreshToken))
                throw new ExceptionResult("El token no fue encontrado", 202);

            var token = _tokenStoreService.FindToken(refreshToken);

            if (token == null)
                throw new ExceptionResult("Usuario no autorizado", 401);

            var user = _authService.FindUser(token.UserId);

            _tokenStoreService.CreateJwtTokens(user);

            return new JsonResult<UserModel>(user);
        }

        [HttpGet("[action]")]
        [CustomAuthorize]
        public JsonResult<bool> Logout()
        {
            var claimsIdentity = User.Identity as ClaimsIdentity;
            var userIdValue = claimsIdentity?.FindFirst(ClaimTypes.UserData)?.Value;

            // Delete the user's tokens from the database (revoke its bearer token)
            if (!string.IsNullOrWhiteSpace(userIdValue) && int.TryParse(userIdValue, out var userId))
                _tokenStoreService.InvalidateUserTokens(userId);

            _tokenStoreService.DeleteExpiredTokens();
            _db.SaveChanges();

            return new JsonResult<bool>(true);
        }

        [HttpPost("[action]")]
        public JsonResult<UserModel> Create([FromBody] UserModel data)
        {
            if (string.IsNullOrEmpty(data?.Password) || string.IsNullOrEmpty(data.Username) ||
                string.IsNullOrEmpty(data.DisplayName))
                throw new ExceptionResult("Usuario o contraseņa son requeridos", 202);

            data.HashPassword();

            var user = _authService.CreateUser(data);
            
            OnCreatedUser(user);

            return new JsonResult<UserModel>(user);
        }

        public virtual void AfterLogin(UserModel data)
        {
        }

        public virtual void OnCreatedUser(UserModel data)
        {
        }
    }
}