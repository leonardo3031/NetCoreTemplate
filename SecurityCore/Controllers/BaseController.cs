﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using SecuritySGD.Entities;
using SecuritySGD.Interfaces;

namespace Security.Controllers
{
    [Route("[controller]")]
    [EnableCors("CorsPolicy")]
    [Produces("application/json", new string[] {})]
    public abstract class BaseController : Controller, IUtility
    {
        protected BaseController(string formName)
        {
            this.FormName = formName;
        }

        protected string ControllerName
        {
            get
            {
                return this.ControllerContext.ActionDescriptor.ControllerName;
            }
        }

        public string FormName { get; set; }

        protected UserModel AuthUser
        {
            get
            {
                ClaimsIdentity identity;
                if ((identity = this.User.Identity as ClaimsIdentity) == null)
                    return new UserModel();
                Claim claim = identity.Claims.FirstOrDefault<Claim>((Func<Claim, bool>) (f => f.Type == "User"));
                return claim != null ? JsonConvert.DeserializeObject<UserModel>(claim.Value) : new UserModel();
            }
        }
    }
}
