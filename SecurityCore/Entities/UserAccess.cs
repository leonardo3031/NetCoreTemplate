using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;

namespace SecuritySGD.Entities
{
    [Table("SECUserAccess")]
    public class UserAccess
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public short Action { get; set; }

        [StringLength(100)]
        public string Description { get; set; }

        public int UserId { get; set; }

        public int AccessFormId { get; set; }

        [JsonIgnore]
        public AccessForm AccessForm { get; set; }

        [JsonIgnore]
        public User User { get; set; }
    }
}