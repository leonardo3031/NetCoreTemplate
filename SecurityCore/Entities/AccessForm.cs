using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;

namespace SecuritySGD.Entities
{
    [Table("SECAccessForm")]
    public class AccessForm
    {
        public AccessForm()
        {
            UserAccess = new HashSet<UserAccess>();
            Menu = new HashSet<Menu>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [StringLength(50)]
        public string FormName { get; set; }

        //public int AccessGroupId { get; set; }

        [StringLength(100)]
        public string Description { get; set; }

        //[JsonIgnore]
        //public AccessGroup AccessGroup { get; set; }

        [JsonIgnore]
        public IEnumerable<UserAccess> UserAccess { get; set; }

        [JsonIgnore]
        public IEnumerable<Menu> Menu { get; set; }
    }
}