using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;
using SecuritySGD.Helper;

namespace SecuritySGD.Entities
{
    [Table("SECUser")]
    public class User
    {
        public User()
        {
            UserAccess = new HashSet<UserAccess>();
            UserToken = new HashSet<UserToken>();
            //UserRole = new HashSet<UserRole>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        [StringLength(100)]
        public string DisplayName { get; set; }

        public bool IsActive { get; set; }

        public DateTimeOffset? LastLoggedIn { get; set; }

        [Required]
        public string Password { get; set; }

        [StringLength(50)]
        public string SerialNumber { get; set; }

        [Required]
        [StringLength(30)]
        public string Username { get; set; }

        [JsonIgnore]
        public IEnumerable<UserAccess> UserAccess { get; set; }

        [JsonIgnore]
        public IEnumerable<UserToken> UserToken { get; set; }

        //[JsonIgnore]
        //public IEnumerable<UserRole> UserRole { get; set; }
    }

    [NotMapped]
    public class UserModel : User
    {
        public string Token { get; set; }
        public string RefreshToken { get; set; }
        
        public object Data { get; set; }

        public User GetEntity()
        {
            return this.ToClass(new User());
        }

        public object GetModel()
        {
            return this;
        }
    }
}