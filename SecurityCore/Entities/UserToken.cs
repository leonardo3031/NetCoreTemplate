using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;
using SecuritySGD.Helper;

namespace SecuritySGD.Entities
{
    [Table("SECUserToken")]
    public class UserToken
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public DateTimeOffset AccessTokenExpiresDateTime { get; set; }

        public string AccessTokenHash { get; set; }

        public DateTimeOffset RefreshTokenExpiresDateTime { get; set; }

        public string RefreshTokenIdHash { get; set; }

        public int UserId { get; set; }

        [JsonIgnore]
        public User User { get; set; }
    }

    [NotMapped]
    public class UserTokenModel : UserToken
    {
        public UserToken ToUserToken()
        {
            return this.ToClass(new UserToken());
        }
    }
}