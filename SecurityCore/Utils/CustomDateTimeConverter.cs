﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace SecuritySGD.Utils
{
    public class CustomDateTimeConverter : DateTimeConverterBase
    {
        public override object ReadJson(JsonReader reader, Type objectType, object existingValue,
            JsonSerializer serializer)
        {
            if (reader.Value == null)
                return null;

            return DateTime.Parse(reader.Value.ToString());
        }
        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            switch (value)
            {
                case null:
                    return;
                case DateTimeOffset offset:
                    writer.WriteValue(offset.LocalDateTime.ToString("dd/MM/yyyy hh:mm:ss"));
                    break;
                default:
                    writer.WriteValue(((DateTime)value).ToString("dd/MM/yyyy hh:mm:ss"));
                    break;
            }
        }
    }

}
