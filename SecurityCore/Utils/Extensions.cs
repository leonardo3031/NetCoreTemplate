﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using SecuritySGD.Entities;

namespace SecuritySGD.Utils
{
    public static class Extensions
    {
        public static UserModel ToUserModel(this User model)
        {
            if (model == null)
                return null;

            return new UserModel
            {
                Id = model.Id,
                DisplayName = model.DisplayName,
                IsActive = model.IsActive,
                LastLoggedIn = model.LastLoggedIn,
                SerialNumber = model.SerialNumber,
                Username = model.Username
            };
        }

        public static UserTokenModel ToUserTokenModel(this UserToken model)
        {
            if (model == null)
                return null;

            return new UserTokenModel
            {
                Id = model.Id,
                AccessTokenExpiresDateTime = model.AccessTokenExpiresDateTime,
                AccessTokenHash = model.AccessTokenHash,
                RefreshTokenExpiresDateTime = model.RefreshTokenExpiresDateTime,
                RefreshTokenIdHash = model.RefreshTokenIdHash,
                UserId = model.UserId
            };
        }

        public static IWebHostBuilder AppSettingsBuilder(this IWebHostBuilder host)
        {
            host.UseKestrel()
                .UseContentRoot(Directory.GetCurrentDirectory())
                .ConfigureAppConfiguration((hostingContext, config) =>
                {
                    var env = hostingContext.HostingEnvironment;
                    config.SetBasePath(env.ContentRootPath);
                    config.AddInMemoryCollection(new[]
                        {
                            new KeyValuePair<string, string>("the-key", "the-value")
                        })
                        .AddJsonFile("appsettings.json", reloadOnChange: true, optional: false)
                        .AddJsonFile($"appsettings.{env}.json", true)
                        .AddEnvironmentVariables();
                })
                .ConfigureLogging((hostingContext, logging) =>
                {
                    logging.AddDebug();
                    logging.AddConsole();
                })
                .UseKestrel()
                .UseContentRoot(Directory.GetCurrentDirectory())
                .UseIISIntegration()
                .UseDefaultServiceProvider((context, options) =>
                {
                    options.ValidateScopes = context.HostingEnvironment.IsDevelopment();
                });

            return host;
        }

        public static string GetClaimsInfo(this HttpContext context, string claim)
        {
            return context.User.Claims.ToList().FirstOrDefault(f => f.Type == claim)?.Value;
        }
    }
}