﻿using System;

namespace SecuritySGD.Utils
{
    [Flags]
    public enum CrudAction
    {
        None = 0,
        Load = 1,
        Create = 2,
        Read = 4,
        Update = 8,
        Delete = 16
    }
}