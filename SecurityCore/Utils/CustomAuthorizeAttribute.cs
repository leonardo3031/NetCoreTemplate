﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.DependencyInjection;
using SecuritySGD.Helper;
using SecuritySGD.Interfaces;
using SecuritySGD.Services;

namespace SecuritySGD.Utils
{
    public class CustomAuthorizeAttribute : TypeFilterAttribute
    {
        public CustomAuthorizeAttribute(string formName, CrudAction action)
            : base(typeof(CustomAuthorizeActionFilter))
        {
            Arguments = new object[] {formName, action};
        }

        public CustomAuthorizeAttribute() : this("", CrudAction.None)
        {
        }

        public CustomAuthorizeAttribute(CrudAction action) : this(string.Empty, action)
        {
        }
    }

    public class CustomAuthorizeActionFilter : IAsyncActionFilter
    {
        public CustomAuthorizeActionFilter(string formName, CrudAction action)
        {
            FormName = formName;
            Action = action;
        }

        private string FormName { get; set; }
        private CrudAction Action { get; }

        public Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            var identity = context.HttpContext.User.Identity;

            if (identity != null && !identity.IsAuthenticated)
                throw new ExceptionResult("Usuario no autorizado");

            if (Action == CrudAction.None) return next();

            var utility = context.Controller as IUtility;

            if (utility != null && AppSetting.GetInstance().DisableSecurity)
                return next();

            if (string.IsNullOrEmpty(FormName) && utility != null)
                FormName = utility.FormName;

            var authService = context.HttpContext.RequestServices.GetRequiredService<IAuthService>();

            int.TryParse(context.HttpContext.GetClaimsInfo(SecurityParams.UserId), out var userId);

            var item = authService.ValidateAccess(FormName, userId);

            if (item == null || !item.CanExecute(Action))
                throw new ExceptionResult(
                    "Accesso denegado, este usuario no cuenta con los privilegios para ejecutar esta acción.");

            return next();
        }
    }
}