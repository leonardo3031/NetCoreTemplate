using System;
using System.Security.Cryptography;
using System.Text;
using SecuritySGD.Entities;

namespace SecuritySGD.Utils
{
    public static class SecurityParams
    {
        public const string UserId = "UserId";
        public const string DisplayName = "DisplayName";
        public const string User = "User";

        public static string GetSha256Hash(string input)
        {
            using (var hashAlgorithm = new SHA256CryptoServiceProvider())
            {
                var byteValue = Encoding.UTF8.GetBytes(input);
                var byteHash = hashAlgorithm.ComputeHash(byteValue);

                return Convert.ToBase64String(byteHash);
            }
        }

        public static void HashPassword(this UserModel user)
        {
            user.Password = GetSha256Hash(user.Password);
            user.SerialNumber = Guid.NewGuid().ToString().Replace("-", "");
            user.IsActive = true;
        }
    }
}