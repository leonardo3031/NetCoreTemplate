﻿namespace SecuritySGD.Utils
{
    public class CustomAccess
    {
        public CrudAction Action { get; set; }
        public string FormName { get; set; }

        public bool CanExecute(CrudAction action)
        {
            return (Action & action) == action;
        }
    }
}