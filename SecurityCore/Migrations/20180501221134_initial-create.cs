﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;

namespace SecuritySGD.Migrations
{
    public partial class initialcreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "SECAccessForm",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Description = table.Column<string>(maxLength: 100, nullable: true),
                    FormName = table.Column<string>(maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SECAccessForm", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "SECUser",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    DisplayName = table.Column<string>(maxLength: 100, nullable: false),
                    IsActive = table.Column<bool>(nullable: false),
                    LastLoggedIn = table.Column<DateTimeOffset>(nullable: true),
                    Password = table.Column<string>(nullable: false),
                    SerialNumber = table.Column<string>(maxLength: 50, nullable: true),
                    Username = table.Column<string>(maxLength: 30, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SECUser", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "SECMenu",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    AccessFormId = table.Column<int>(nullable: true),
                    Data = table.Column<string>(nullable: true),
                    Name = table.Column<string>(maxLength: 30, nullable: false),
                    ParentId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SECMenu", x => x.Id);
                    table.ForeignKey(
                        name: "FK_SECMenu_SECAccessForm_AccessFormId",
                        column: x => x.AccessFormId,
                        principalTable: "SECAccessForm",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "SECUserAccess",
                columns: table => new
                {
                    Action = table.Column<short>(nullable: false),
                    AccessFormId = table.Column<int>(nullable: false),
                    UserId = table.Column<int>(nullable: false),
                    Description = table.Column<string>(maxLength: 100, nullable: true),
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SECUserAccess", x => new { x.Action, x.AccessFormId, x.UserId });
                    table.ForeignKey(
                        name: "FK_SECUserAccess_SECAccessForm_AccessFormId",
                        column: x => x.AccessFormId,
                        principalTable: "SECAccessForm",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_SECUserAccess_SECUser_UserId",
                        column: x => x.UserId,
                        principalTable: "SECUser",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "SECUserToken",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    AccessTokenExpiresDateTime = table.Column<DateTimeOffset>(nullable: false),
                    AccessTokenHash = table.Column<string>(nullable: true),
                    RefreshTokenExpiresDateTime = table.Column<DateTimeOffset>(nullable: false),
                    RefreshTokenIdHash = table.Column<string>(nullable: true),
                    UserId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SECUserToken", x => x.Id);
                    table.ForeignKey(
                        name: "FK_SECUserToken_SECUser_UserId",
                        column: x => x.UserId,
                        principalTable: "SECUser",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_SECAccessForm_FormName",
                table: "SECAccessForm",
                column: "FormName",
                unique: true,
                filter: "[FormName] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_SECMenu_AccessFormId",
                table: "SECMenu",
                column: "AccessFormId");

            migrationBuilder.CreateIndex(
                name: "IX_SECUser_Username",
                table: "SECUser",
                column: "Username",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_SECUserAccess_AccessFormId",
                table: "SECUserAccess",
                column: "AccessFormId");

            migrationBuilder.CreateIndex(
                name: "IX_SECUserAccess_UserId",
                table: "SECUserAccess",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_SECUserToken_UserId",
                table: "SECUserToken",
                column: "UserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "SECMenu");

            migrationBuilder.DropTable(
                name: "SECUserAccess");

            migrationBuilder.DropTable(
                name: "SECUserToken");

            migrationBuilder.DropTable(
                name: "SECAccessForm");

            migrationBuilder.DropTable(
                name: "SECUser");
        }
    }
}
