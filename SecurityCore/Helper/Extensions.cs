﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Reflection;
using Microsoft.EntityFrameworkCore;

namespace SecuritySGD.Helper
{
    public static class Extensions
    {
        private static string GetIdentityProperty<T>()
        {
            var prop = typeof(T).GetProperties()
                .ToList()
                .FirstOrDefault(w =>
                    w.GetCustomAttribute<DatabaseGeneratedAttribute>()?.DatabaseGeneratedOption ==
                    DatabaseGeneratedOption.Identity);

            if (prop == null)
                throw new Exception("Propiedad de identidad no encontrada.");
            return prop.Name;
        }

        /// <returns>Date converted to seconds since Unix epoch (Jan 1, 1970, midnight UTC).</returns>
        public static long ToUnixEpochDate(this DateTime date)
        {
            return (long) Math.Round((date.ToUniversalTime() -
                                      new DateTimeOffset(1970, 1, 1, 0, 0, 0, TimeSpan.Zero)).TotalSeconds);
        }

        public static void CheckArgumentIsNull(this object o, string name)
        {
            if (o == null)
                throw new ArgumentNullException(name);
        }

        public static void UseDbDriver(this DbContextOptionsBuilder builder)
        {
            var setting = AppSetting.GetInstance();
            var timeOut = setting.MinutesTimeOut;
            var minutesTimeOut = (int) TimeSpan.FromMinutes(timeOut == 0 ? 1 : timeOut).TotalSeconds;

            builder.UseSqlServer(AppSetting.GetActiveConnectionDb(),
                serverDbContextOptionsBuilder =>
                {
                    serverDbContextOptionsBuilder.CommandTimeout(minutesTimeOut);
                    serverDbContextOptionsBuilder.EnableRetryOnFailure();
                });
        }
    }
}