﻿using System;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Cors.Internal;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using SecuritySGD.DataLayer;
using SecuritySGD.Services;
using SecuritySGD.Utils;

namespace SecuritySGD.Helper
{
    public static class SecurityStartup
    {
        public static void ConfigureJWT(this IServiceCollection services, IConfiguration Configuration)
        {
            services.AddSingleton(Configuration);
            services.AddScoped<IAuthService, AuthService>();
            services.AddScoped<SecurityInitializerService>();
            services.AddScoped<ITokenStoreService, TokenStoreService>();

            services.AddDbContext<SecurityDbContext>(options => { options.UseDbDriver(); });

            // Needed for jwt auth.
            services.Configure<BearerTokensOptions>(options => Configuration.GetSection("BearerTokens").Bind(options));
            services
                .AddAuthentication(options =>
                {
                    options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                    options.DefaultSignInScheme = JwtBearerDefaults.AuthenticationScheme;
                    options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                })
                .AddJwtBearer(cfg =>
                {
                    cfg.RequireHttpsMetadata = false;
                    cfg.SaveToken = true;
                    cfg.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidIssuer = Configuration["BearerTokens:Issuer"],
                        ValidAudience = Configuration["BearerTokens:Audience"],
                        IssuerSigningKey =
                            new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration["BearerTokens:Key"])),
                        ValidateIssuerSigningKey = true,
                        ValidateIssuer = true,
                        ValidateLifetime = true,
                        ClockSkew = TimeSpan.Zero
                    };
                    cfg.Events = new JwtBearerEvents
                    {
                        OnAuthenticationFailed = context =>
                        {
                            if (context.Exception is ExceptionResult)
                                return Task.FromException(context.Exception);

                            return Task.FromException(new ExceptionResult(context.Exception, 401));
                        },
                        OnTokenValidated = context =>
                        {
                            var authService = context.HttpContext.RequestServices
                                .GetRequiredService<IAuthService>();

                            authService.Validate(context);

                            return Task.CompletedTask;
                        },
                        OnMessageReceived = context => Task.CompletedTask,
                        OnChallenge = context =>
                        {
                            var logger = context.HttpContext.RequestServices.GetRequiredService<ILoggerFactory>()
                                .CreateLogger(nameof(JwtBearerEvents));
                            logger.LogError("OnChallenge error", context.Error, context.ErrorDescription);
                            return Task.CompletedTask;
                        }
                    };
                });
        }

        public static void ConfigureErrorHandler(this IApplicationBuilder app)
        {
            app.UseExceptionHandler(appBuilder =>
            {
                appBuilder.Use(async (context, next) =>
                {
                    var error = context.Features[typeof(IExceptionHandlerFeature)] as IExceptionHandlerFeature;

                    context.Response.ContentType = "application/json";
                    context.Response.Headers.Add("Access-Control-Allow-Origin", "*");
                    context.Response.Headers.Add("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE");
                    context.Response.Headers.Add("Access-Control-Max-Age", "3600");
                    context.Response.Headers.Add("Access-Control-Allow-Headers", "x-requested-with");

                    if (error != null)
                        switch (error.Error)
                        {
                            case SecurityTokenExpiredException _:
                                context.Response.StatusCode = 401;

                                await context.Response
                                    .WriteAsync(
                                        JsonConvert.SerializeObject(new
                                        {
                                            new ExceptionResult("El token ha expirado", 401).Data
                                        }))
                                    .ConfigureAwait(false);
                                break;
                            case SecurityTokenDecryptionFailedException _:
                                await context.Response
                                    .WriteAsync(
                                        JsonConvert.SerializeObject(
                                            new ExceptionResult("Error al desencriptar el token")))
                                    .ConfigureAwait(false);
                                break;
                            case ExceptionResult _:
                                var exception = ((ExceptionResult) error.Error).Data;
                                context.Response.StatusCode = exception.HttpCode;

                                await context.Response
                                    .WriteAsync(JsonConvert.SerializeObject(new {Data = exception}))
                                    .ConfigureAwait(false);
                                break;
                            default:
                                if (error.Error != null)
                                {
                                    context.Response.StatusCode = 500;

                                    await context.Response
                                        .WriteAsync(JsonConvert.SerializeObject(new
                                        {
                                            new ExceptionResult(error.Error.Message).Data
                                        }))
                                        .ConfigureAwait(false);
                                }

                                break;
                        }
                    else
                        await next().ConfigureAwait(false);
                });
            });
        }

        public static void SeedSecurity(this IApplicationBuilder app)
        {
            var scopeFactory = app.ApplicationServices.GetRequiredService<IServiceScopeFactory>();

            //SEED DB
            using (var scope = scopeFactory.CreateScope())
            {
                var dbInitializer = scope.ServiceProvider.GetService<SecurityInitializerService>();

                dbInitializer.Initialize();
                dbInitializer.SeedData();
            }
        }
    }
}