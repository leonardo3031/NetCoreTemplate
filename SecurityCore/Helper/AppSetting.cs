﻿using System;
using System.Collections.Generic;
using System.IO;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace SecuritySGD.Helper
{
    public class AppSetting
    {
        public AppSetting()
        {
            DbList = new List<string>();
        }

        public string AppName { get; set; }
        public bool DisableSecurity { get; set; }
        public List<string> DbList { get; set; }
        public string DefaultDb { get; set; }
        public double MinutesTimeOut { get; set; }
        public string ConnectionName { get; set; }
        public string DefaultCulture { get; set; }

        public static IConfigurationRoot GetConfiguration()
        {
            return new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json")
                .Build();
        }

        public static AppSetting GetInstance()
        {
            var setting = new AppSetting();

            GetConfiguration().GetSection("AppSettings").Bind(setting);

            return setting;
        }

        public static string GetActiveConnectionDb()
        {
            var configuration = GetConfiguration();
            var connectionName = GetInstance().ConnectionName ?? "Default";

            var connection = configuration.GetSection("ConnectionStrings")[connectionName];

            if (string.IsNullOrEmpty(connection) || string.IsNullOrWhiteSpace(connection))
                throw new Exception(
                    $"No se encontró la cadena de conexión {connectionName} en la base de datos.");

            return connection;
        }
    }

    public class App
    {
        public static T GetContext<T>() where T : DbContext
        {
            var builder = new DbContextOptionsBuilder<T>();

            builder.UseDbDriver();

            var context = (T) Activator.CreateInstance(typeof(T), builder.Options);

            return context;
        }
    }
}