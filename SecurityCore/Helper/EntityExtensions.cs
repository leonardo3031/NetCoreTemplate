﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using Microsoft.EntityFrameworkCore;

namespace SecuritySGD.Helper
{
    public static class EntityExtensions
    {
        private static string GetEntityName<TEntity>()
        {
            var t = typeof(TEntity).GetCustomAttribute<TableAttribute>();
            return t != null ? t.Name : typeof(TEntity).Name;
        }

        private static int GetPropertyStringLength<T>(this T model, string propertyName) where T : class
        {
            var att = typeof(T).GetCustomAttribute<StringLengthAttribute>();
            if (att == null)
                return 20;
            return att.MaximumLength;
        }


        public static TTarget ToClass<TSource, TTarget>(this TSource source, TTarget target)
        {
            return Utils.CopyTo(source, target);
        }

        public static TTarget ToClass<TSource, TTarget>(this TSource source)
        {
            var target = (TTarget) Activator.CreateInstance(typeof(TTarget), new object[] { });

            return Utils.CopyTo(source, target);
        }

        public static void SetState<TEntity>(this DbContext db, TEntity entity, Expression<Func<TEntity, object>> key)
            where TEntity : class
        {
            if (!(((UnaryExpression) key.Body).Operand is MemberExpression expr)) return;

            var member = expr.Member.Name;
            var prop = typeof(TEntity).GetProperty(member);

            if (!new[]
                {
                    typeof(int),
                    typeof(short),
                    typeof(long),
                    typeof(byte)
                }
                .Contains(prop.PropertyType))
                throw new Exception($"E001: La propiedad {member} debe ser numérica");

            var keyvalue = key.Compile()(entity);

            int.TryParse(keyvalue.ToString(), out var value);

            prop.SetValue(entity, Math.Abs(value));

            EntityState state;

            if (value < 0)
                state = EntityState.Deleted;
            else
                state = value == 0 ? EntityState.Added : EntityState.Modified;

            db.Entry(entity).State = state;
        }

        public static void ExcludeFromUpdate<TEntity>(this DbSet<TEntity> dbSet, TEntity entity,
            params Expression<Func<TEntity, object>>[] keys) where TEntity : class
        {
            var db = GetDb(dbSet);

            foreach (var key in keys) db.Entry(entity).Property(key).IsModified = false;
        }

        private static DbContext GetDb<TEntity>(DbSet<TEntity> set) where TEntity : class
        {
            return (DbContext) set
                .GetType().GetTypeInfo()
                .GetField("_context", BindingFlags.NonPublic | BindingFlags.Instance)
                .GetValue(set);
        }

        public static string GetMemberName<TSource, TField>(Expression<Func<TSource, TField>> field)
        {
            return (field.Body as MemberExpression ?? (MemberExpression) ((UnaryExpression) field.Body).Operand).Member
                .Name;
        }

        public static void AddOrUpdate<TEntity>(this DbSet<TEntity> dbSet, Expression<Func<TEntity, object>> key,
            params TEntity[] listData)
            where TEntity : class
        {
            var context = GetDb(dbSet);
            var ids = context.Model.FindEntityType(typeof(TEntity)).FindPrimaryKey().Properties.Select(x => x.Name);
            var t = typeof(TEntity);

            foreach (var data in listData)
            {
                var keyObject = key.Compile()(data);
                var keyFields = keyObject.GetType().GetProperties().Select(p => t.GetProperty(p.Name)).ToArray();
                var keyVals = keyFields.Select(p => p.GetValue(data));
                var entities = dbSet.AsNoTracking().ToList();
                var i = 0;

                foreach (var keyVal in keyVals)
                {
                    entities = entities
                        .Where(p => p.GetType().GetProperty(keyFields[i].Name).GetValue(p).Equals(keyVal)).ToList();

                    i++;
                }

                if (entities.Any())
                {
                    var dbVal = entities.FirstOrDefault();
                    var keyAttrs = data.GetType().GetProperties().Where(p => ids.Contains(p.Name)).ToList();

                    if (keyAttrs.Any())
                    {
                        foreach (var keyAttr in keyAttrs)
                            keyAttr.SetValue(data,
                                dbVal?.GetType().GetProperties().FirstOrDefault(p => p.Name == keyAttr.Name)
                                    ?.GetValue(dbVal));

                        context.Entry(dbVal ?? throw new InvalidOperationException()).CurrentValues.SetValues(data);
                        context.Entry(dbVal).State = EntityState.Modified;

                        return;
                    }
                }

                dbSet.Add(data);
            }
        }
    }

    public static class Utils
    {
        public static TK CopyTo<T, TK>(T source, TK target)
        {
            var tprops = typeof(TK).GetProperties();

            foreach (var tprop in tprops)
            {
                var sprop = source.GetType().GetProperty(tprop.Name);

                if (sprop == null)
                {
                    var attr = (InternalNameAttribute) tprop.GetCustomAttribute(typeof(InternalNameAttribute));

                    if (attr != null)
                        sprop = source.GetType().GetProperty(attr.Name);
                }

                if (sprop != null && sprop.PropertyType == tprop.PropertyType)
                    tprop.SetValue(target, sprop.GetValue(source));
            }

            return target;
        }
    }

    public class InternalNameAttribute : Attribute
    {
        public InternalNameAttribute(string name)
        {
            Name = name;
        }

        public string Name { get; set; }
    }
}