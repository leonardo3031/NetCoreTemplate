﻿using System;
using System.Data.SqlClient;

namespace SecuritySGD.Helper
{
    public class JsonResult<T>
    {
        public JsonResult(T dataSource, string message = "Success", bool commit = true, int totalItems = 1,
            int stateCode = 200)
        {
            Data = new Response<T>
            {
                DataSource = dataSource,
                Commit = commit,
                Message = message,
                TotalItems = totalItems,
                HttpCode = stateCode
            };
        }

        public JsonResult(T dataSource, int totalItems, int stateCode = 200)
            : this(dataSource, "Success", true, totalItems)
        {
        }

        public Response<T> Data { get; }
    }

    public class ExceptionResult : Exception
    {
        public ExceptionResult(string message = "Internal Server Error", int httpCode = 500)
        {
            Data = new Response<object>
            {
                Commit = false,
                Message = message,
                HttpCode = httpCode
            };
        }

        public ExceptionResult(Exception e)
        {
            var message = string.Empty;
            var sqlError = e.InnerException as SqlException;

            if (sqlError?.Errors.Count > 0)
                switch (sqlError.Errors[0].Number)
                {
                    case 547:
                        //TODO: Corregir
                        message +=
                            $"No se puede eliminar este registro, ya que esta asociado a otro(s) registro(s).<br />{sqlError.Message}";
                        break;
                    default:
                        message += sqlError.Message;
                        break;
                }

            if (string.IsNullOrEmpty(message))
                message = e.Message + "<br />";

            Data = new Response<object>
            {
                Commit = false,
                Message = message,
                HttpCode = 500
            };
        }

        public ExceptionResult(Exception e, int httpCode = 500)
        {
            var message = string.Empty;
            var sqlError = e.InnerException?.InnerException as SqlException;

            if (sqlError?.Errors.Count > 0)
                switch (sqlError.Errors[0].Number)
                {
                    case 547:
                        //TODO: Corregir
                        message +=
                            $"No se puede eliminar este registro, ya que esta asociado a otro(s) registro(s).<br />{sqlError.Message}";
                        break;
                    default:
                        message += sqlError.Message;
                        break;
                }

            if (string.IsNullOrEmpty(message))
                message = e.Message + "<br />";

            Data = new Response<object>
            {
                Commit = false,
                Message = message,
                HttpCode = httpCode
            };
        }

        public new Response<object> Data { get; }
    }

    public class Response<T>
    {
        public T DataSource { get; set; }
        public bool Commit { get; set; }
        public string Message { get; set; }
        public int TotalItems { get; set; }
        public int HttpCode { get; set; }
    }
}