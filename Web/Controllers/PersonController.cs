﻿using System;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Model.Models;
using SecuritySGD.Utils;
using Service;
using Service.Utils;
using Web.Models;
using Web.Utils;

namespace Web.Controllers
{
    public class PersonController : Controller//: SecurityController<PersonService>
    {
        private readonly PersonService _personService;

        public PersonController(PersonService personService)// : base(personService, "PERSONA")
        {
            _personService = personService;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Save()
        {
            var newPerson = new PersonViewModel
            {
                BirthDate = DateTime.Today
            };
            return View(newPerson);
        }
        
        public IActionResult Save(PersonViewModel model)
        {
            var person = model.GetModel(new Person());

            if (!ModelState.IsValid)
            {
                return View(model);
            }

            _personService.SavePerson(person);
            _personService.SaveChanges();

            var persons = _personService.GetPersons();
            model.Persons = persons;

            return RedirectToAction("Index");
        }
        
        public IActionResult Edit(PersonViewModel model)
        {
            return View("Save", model);
        }
        
        public IActionResult Delete(PersonViewModel model)
        {

            var person = model.GetModel(new Person());

            _personService.DeletePerson(person);
            _personService.SaveChanges();

            var persons = _personService.GetPersons();
            model.Persons = persons;

            return RedirectToAction("Index");
        }
    }

    public class PersonApiController : SecurityController<PersonService>
    {
        private readonly PersonService _personService;

        public PersonApiController(PersonService personService) : base(personService, "PERSON")
        {
            _personService = personService;
        }

        [HttpGet]
        [Route("[action]")]
        [CustomAuthorize(CrudAction.Read)]
        public IDataResult GetPersons()
        {
            try
            {
                return new CustomResult(_personService.GetPersons());
            }
            catch (Exception e)
            {
                throw new ExceptionResult(e);
            }
        }
    }
}