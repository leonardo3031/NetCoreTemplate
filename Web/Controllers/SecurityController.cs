﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Filters;
using Newtonsoft.Json;
using Security.Controllers;
using SecuritySGD.Controllers;
using Service;
using Service.Utils;

namespace Web.Controllers
{
    public class SecurityController<T> : BaseController where T : SecurityService
    {
        private readonly T _service;

        public SecurityController(T service, string formName) : base(formName)
        {
            this._service = service;
        }

        public GlobalConfig GlobalConfig { get; set; }

        public override void OnActionExecuting(ActionExecutingContext context)
        {
            var gc = JsonConvert.DeserializeObject<GlobalConfig>(AuthUser.Data.ToString());

            GlobalConfig = gc;
            _service.GlobalConfig = GlobalConfig;
        }
    }
}
