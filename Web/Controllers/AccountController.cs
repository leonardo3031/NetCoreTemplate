﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SecuritySGD.Controllers;
using SecuritySGD.DataLayer;
using SecuritySGD.Entities;
using SecuritySGD.Services;
using Service.Utils;

namespace Web.Controllers
{
    public class AccountController : SecurityController
    {
        private readonly SecurityDbContext _securityContext;

        public AccountController(IAuthService authService, ITokenStoreService tokenStoreService, SecurityDbContext context) : base(authService, tokenStoreService, context)
        {
            _securityContext = context;
        }

        public override void AfterLogin(UserModel user)
        {
            var gc = new GlobalConfig
            {
                Nombre = user.DisplayName
            };
            user.Data = gc;
        }
    }
}
