﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Web.Utils
{
    public class DataResult
    {
        public object DataSource { get; set;}
        public string Message { get; set;}
        public bool Commit { get; set;}
        public int TotalItems { get; set;}
    }

    public interface IDataResult
    {
        DataResult Data { get; set; }
    }

    public class CustomResult : IDataResult
    {
        public DataResult Data { get; set; }
        public CustomResult(object dataSource, string message, bool commit, int totalItems)
        {
            this.Data = new DataResult
            {
                DataSource = dataSource,
                Message = message,
                Commit = commit,
                TotalItems = totalItems,
            };
            
        }
        public CustomResult(object data, int totalItems) : this (data, "", true, totalItems)
        {
        }
        public CustomResult(object data) : this (data, 0)
        {
        }
        public static IDataResult Result(object data, int totalItems)
        {
            return new CustomResult(data, totalItems);
        }
    }

    public class ExceptionResult : Exception, IDataResult
    {
        public DataResult Data { get; set; }

        public ExceptionResult(Exception e)
        {
            this.Data = new DataResult
            {
                DataSource = null,
                Message = e.Message,
                Commit = false,
                TotalItems = 0
            };
        }

        public static IDataResult Result(Exception e)
        {
            return new ExceptionResult(e);
        }
    }
}
