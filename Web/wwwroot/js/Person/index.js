﻿new Vue({
    el: "#app",
    data: {
        Persons: []
    },
    methods: {
        getPersons: function() {
            var vm = this;
            var url = prefijo + "PersonApi/GetPersons";
            axios.get(url, {
                headers: {
                    "Authorization": "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJqdGkiOiI0NWFhZmFhMi0wODI3LTRhZTctYWM2MC0xZGIwYTM4YWEwN2EiLCJpc3MiOiJodHRwOi8vbG9jYWxob3N0LyIsImlhdCI6MTU0OTM5NDMzNiwiaHR0cDovL3NjaGVtYXMueG1sc29hcC5vcmcvd3MvMjAwNS8wNS9pZGVudGl0eS9jbGFpbXMvbmFtZWlkZW50aWZpZXIiOiIxIiwiaHR0cDovL3NjaGVtYXMueG1sc29hcC5vcmcvd3MvMjAwNS8wNS9pZGVudGl0eS9jbGFpbXMvbmFtZSI6InNhIiwiVXNlcklkIjoiMSIsIkRpc3BsYXlOYW1lIjoiU3lzdGVtIEFkbWluaXN0cmF0b3IiLCJVc2VyIjoie1wiVG9rZW5cIjpudWxsLFwiUmVmcmVzaFRva2VuXCI6bnVsbCxcIkRhdGFcIjp7XCJOb21icmVcIjpcIlN5c3RlbSBBZG1pbmlzdHJhdG9yXCJ9LFwiSWRcIjoxLFwiRGlzcGxheU5hbWVcIjpcIlN5c3RlbSBBZG1pbmlzdHJhdG9yXCIsXCJJc0FjdGl2ZVwiOnRydWUsXCJMYXN0TG9nZ2VkSW5cIjpudWxsLFwiUGFzc3dvcmRcIjpudWxsLFwiU2VyaWFsTnVtYmVyXCI6XCJmZWVlZDU1MTE3ODg0MzA3OWM1ZTRiZWFiNTFmYzE3MVwiLFwiVXNlcm5hbWVcIjpcInNhXCJ9IiwiaHR0cDovL3NjaGVtYXMubWljcm9zb2Z0LmNvbS93cy8yMDA4LzA2L2lkZW50aXR5L2NsYWltcy9zZXJpYWxudW1iZXIiOiJmZWVlZDU1MTE3ODg0MzA3OWM1ZTRiZWFiNTFmYzE3MSIsImh0dHA6Ly9zY2hlbWFzLm1pY3Jvc29mdC5jb20vd3MvMjAwOC8wNi9pZGVudGl0eS9jbGFpbXMvdXNlcmRhdGEiOiIxIiwibmJmIjoxNTQ5Mzk0MzM2LCJleHAiOjE1NDk0ODA3MzYsImF1ZCI6IkFueSJ9.38xHaC4HkMroUlZnxkGq8Wi7XpBWKM3hoYl_aY9Kk5A"
                }
            }).then(function(result) {
                vm.Persons = result.data.Data.DataSource;
            });
        }
    },
    beforeMount: function() {
        this.getPersons();
    }
});