﻿using System.Collections.Generic;
using System.Linq;
using Data;
using Data.Infrastructure;
using Microsoft.EntityFrameworkCore;
using Model.Models;
using EntityState = Microsoft.EntityFrameworkCore.EntityState;

namespace Service
{
    public interface IPersonService
    {
        IEnumerable<Person> GetPersons();
        IEnumerable<Person> GetPersonsByName(string name);
        Person GetPerson(int id);
        void SavePerson(Person person);
        void DeletePerson(Person person);
        void SaveChanges();
    }

    public class PersonService : SecurityService, IPersonService
    {
        private readonly ApplicationDbContext _db;
        private readonly DbSet<Person> _persons;

        public PersonService(IDbFactory db)
        {
            _db = db.Init();
            _persons = db.Init().Persons;
        }

        public IEnumerable<Person> GetPersons()
        {
            return _persons.ToList();
        }

        public IEnumerable<Person> GetPersonsByName(string name)
        {
            return _persons.Where(x => x.Name.Contains(name));
        }

        public Person GetPerson(int id)
        {
            return _persons.Find(id);
        }

        public void SavePerson(Person person)
        {
            _db.Entry(person).State = person.Id == 0 ? EntityState.Added : EntityState.Modified;
        }

        public void DeletePerson(Person person)
        {
            _persons.Remove(person);
        }

        public void SaveChanges()
        {
            _db.SaveChanges();
        }
    }
}
