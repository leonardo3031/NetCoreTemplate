﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Service.Utils
{
    public static class Extension
    {
        public static K GetModel<T, K>(this T source, K target) where T : class where K : class
        {
            var psource = typeof(T).GetProperties();

            foreach (var item in psource)
            {
                var pt = typeof(K).GetProperty(item.Name);

                if (pt != null)
                {
                    pt.SetValue(target, item.GetValue(source));
                }
            }
            return target;
        }
    }
}
